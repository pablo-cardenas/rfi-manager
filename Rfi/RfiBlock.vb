﻿Imports Autodesk.AutoCAD.DatabaseServices
Imports Autodesk.AutoCAD.ApplicationServices
Imports System.IO
Imports System.Text.RegularExpressions

Public Class RfiBlock
    Inherits Rfi

    'TODO: Optimize the setters
    Public Overrides Property NumRfi() As String
        Get
            Return MyBase.NumRfi
        End Get
        Set(value As String)
            MyBase.NumRfi = value
            Dim db = HostApplicationServices.WorkingDatabase
            Dim blkRefId = db.GetObjectId(False, New Handle(_Handle), 0)
            Using tr = db.TransactionManager.StartTransaction()
                Dim blkRef As BlockReference = tr.GetObject(blkRefId, OpenMode.ForRead)
                For Each arId In blkRef.AttributeCollection
                    Dim ar As AttributeReference = tr.GetObject(arId, OpenMode.ForRead)
                    If ar IsNot Nothing AndAlso ar.Tag.ToUpper() = "NUMRFI" Then
                        ar.UpgradeOpen()
                        ar.TextString = MyBase.NumRfi
                        ar.DowngradeOpen()
                    End If
                Next
                tr.Commit()
            End Using
        End Set
    End Property

    Public Overrides Property Description() As String
        Get
            Return MyBase.Description
        End Get
        Set(value As String)
            MyBase.Description = value
            Dim db = HostApplicationServices.WorkingDatabase
            Dim blkRefId = db.GetObjectId(False, New Handle(_Handle), 0)
            Using tr = db.TransactionManager.StartTransaction()
                Dim blkRef As BlockReference = tr.GetObject(blkRefId, OpenMode.ForRead)
                For Each arId In blkRef.AttributeCollection
                    Dim ar As AttributeReference = tr.GetObject(arId, OpenMode.ForRead)
                    If ar IsNot Nothing AndAlso (ar.Tag.ToUpper() = "DESCRIPTION" OrElse ar.Tag.ToUpper = "DESCRIPCIÓN") Then
                        ar.UpgradeOpen()
                        ar.TextString = MyBase.Description
                        ar.DowngradeOpen()
                    End If
                Next
                tr.Commit()
            End Using
        End Set
    End Property

    Public Overrides Property Status() As String
        Get
            Return MyBase.Status
        End Get
        Set(value As String)
            MyBase.Status = value
            Dim db = HostApplicationServices.WorkingDatabase
            Dim blkRefId = db.GetObjectId(False, New Handle(_Handle), 0)
            Using tr = db.TransactionManager.StartTransaction()
                Dim blkRef As BlockReference = tr.GetObject(blkRefId, OpenMode.ForRead)
                For Each arId In blkRef.AttributeCollection
                    Dim ar As AttributeReference = tr.GetObject(arId, OpenMode.ForRead)
                    If ar IsNot Nothing AndAlso (ar.Tag.ToUpper() = "STATUS" OrElse ar.Tag.ToUpper = "ESTADO") Then
                        ar.UpgradeOpen()
                        ar.TextString = MyBase.Status
                        ar.DowngradeOpen()
                    End If
                Next
                tr.Commit()
            End Using
        End Set
    End Property

    Public Sub New(handle As Handle)
        Dim absolutePath = Application.DocumentManager.MdiActiveDocument.Name
        Dim r As New Regex("(?<Discipline>\w+)\\RECIBIDOS\\(?<DwgName>.+)$")
        Dim m = r.Match(absolutePath)
        If m.Success Then
            Discipline = m.Groups("Discipline").Value
            DwgName = m.Groups("DwgName").Value
        End If

        _Handle = handle.Value
        Dim db = HostApplicationServices.WorkingDatabase
        Dim blkRefId = db.GetObjectId(False, handle, 0)
        Using tr = db.TransactionManager.StartOpenCloseTransaction()
            Dim blkRef As BlockReference = tr.GetObject(blkRefId, OpenMode.ForRead)

            'TODO: optimize the following code
            For Each attId In blkRef.AttributeCollection
                Dim attRef As AttributeReference = tr.GetObject(attId, OpenMode.ForRead)
                Select Case attRef.Tag
                    Case "NUMRFI"
                        MyBase.NumRfi = attRef.TextString
                    Case "DESCRIPCIÓN"
                        MyBase.Description = attRef.TextString
                    Case "DESCRIPTION"
                        MyBase.Description = attRef.TextString
                    Case "ESTADO"
                        MyBase.Status = attRef.TextString
                    Case "STATUS"
                        MyBase.Status = attRef.TextString
                End Select
            Next
            tr.Commit()
        End Using
    End Sub
End Class

﻿Imports System.Collections.Generic
Imports Microsoft.Office.Interop.Excel
Imports Autodesk.AutoCAD

Public Class Excel
    Implements IDisposable

    Private Log = ""

    Private excelApp As Application
    Private excelWB As Workbook
    Private excelWS As Worksheet

    Private Const STARTROW = 9
    Private Const STARTCOLUMN = 2
    Public Shared ReadOnly COLUMNS As List(Of String) = New List(Of String)({"NumRfi", "Date", "Description", "Discipline", "Status", "DwgName", "ZoomCommand", "Handle"})

    Private fileName

    Public Sub New(fileName As String)
        Me.fileName = fileName
        excelApp = CreateObject("Excel.Application")
        Try
            excelWB = excelApp.Workbooks.Open(fileName)
        Catch ex As System.Runtime.InteropServices.COMException
            excelWB = excelApp.Workbooks.Add()
        End Try
        excelWS = excelWB.Worksheets(1)
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        excelApp.DisplayAlerts = False
        excelWB.SaveAs(fileName, AccessMode:=XlSaveAsAccessMode.xlExclusive, ConflictResolution:=XlSaveConflictResolution.xlLocalSessionChanges)
        excelApp.Workbooks.Close()
        excelApp.Quit()
        excelApp = Nothing

        Dim ed = ApplicationServices.Core.Application.DocumentManager.MdiActiveDocument.Editor
        ed.WriteMessage(Log)
    End Sub

    Public Sub RegisterRfiList(rfiList As IEnumerable(Of Rfi))
        Dim oldRfiList = GetRfiList()
        Using cw As New ChangesWindow(Me, rfiList, oldRfiList)
            cw.ShowDialog()
        End Using
    End Sub

    Public Sub AppendRfi(rb As RfiBlock)
        Dim nextNumRfi = LastNumRfi() + 1
        Dim range = GetRange(nextNumRfi)
        range(COLUMNS.IndexOf("Status") + 1) = Rfi.POSSIBLESTATUS(0)
        range(COLUMNS.IndexOf("NumRfi") + 1) = nextNumRfi
        range(COLUMNS.IndexOf("Handle") + 1) = rb.Handle
        range(Excel.COLUMNS.IndexOf("ZoomCommand") + 1) = "=""z o (handent """""" &" & range(Excel.COLUMNS.IndexOf("Handle") + 1).Address(False, False) & "& """""") """
        range(Excel.COLUMNS.IndexOf("Date") + 1) = Date.Now().ToString("dd/MM/yyyy")
        Dim rr = GetRfi(nextNumRfi)
        rb.NumRfi = nextNumRfi
        For Each prop In Rfi.PROPLIST
            Dim value = GetType(Rfi).GetProperty(prop).GetValue(rb)
            GetType(Rfi).GetProperty(prop).SetValue(rr, value)
        Next
    End Sub

    Public Function LastNumRfi() As Integer
        Dim last = excelWS.Cells(excelWS.Rows.Count, STARTCOLUMN).End(XlDirection.xlUp).Row - STARTROW + 1
        Return If(last < 0, 0, last)
    End Function

    Public Function GetRfiList() As List(Of RfiRange)
        Dim rfiList As List(Of RfiRange) = New List(Of RfiRange)
        For i = 1 To LastNumRfi()
            rfiList.Add(GetRfi(i))
        Next
        Return rfiList
    End Function

    Public Function GetRfi(numRfi As Integer) As RfiRange
        Return New RfiRange(GetRange(numRfi))
    End Function

    Public Function GetRange(numRfi As Integer) As Range
        Dim row = numRfi + STARTROW - 1
        Return excelWS.Range(excelWS.Cells(row, STARTCOLUMN), excelWS.Cells(row, STARTCOLUMN + COLUMNS.Count - 1))
    End Function
End Class
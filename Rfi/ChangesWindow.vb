﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Windows.Forms

Public Class ChangesWindow
    Private _Excel As Excel

    Private newList As List(Of Rfi)
    Private oldList As List(Of Rfi)

    Private upToDateList As List(Of Rfi)
    Private addList As List(Of Rfi)
    Private updateList As List(Of Rfi)
    Private badNumberList As List(Of Rfi)
    Private changedNumberList As List(Of Rfi)
    Private conflictList As List(Of Rfi)

    Public Sub New(excel As Excel, newEmu As IEnumerable(Of Rfi), oldEnu As IEnumerable(Of Rfi))
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _Excel = excel

        newList = newEmu.ToList
        oldList = oldEnu.ToList

        Dim SameNumRfi As Func(Of Rfi, Rfi, Boolean) = Function(rn, ro) rn.NumRfi = ro.NumRfi
        Dim SameHandle As Func(Of Rfi, Rfi, Boolean) = Function(rn, ro) _
            rn.Handle = ro.Handle AndAlso
            rn.DwgName = ro.DwgName AndAlso
            rn.Discipline = ro.Discipline
        Dim SameContent As Func(Of Rfi, Rfi, Boolean) = Function(rn, ro) _
            rn.Description = ro.Description AndAlso
            rn.Status = ro.Status
        Dim ExistsHandleList = newList.FindAll(Function(rn) oldList.Exists(Function(ro) SameHandle(rn, ro)))
        Dim NotExistsHandleList = newList.FindAll(Function(rn) Not oldList.Exists(Function(ro) SameHandle(rn, ro)))

        'Success
        upToDateList = ExistsHandleList.FindAll(Function(rn) oldList.Exists(Function(ro) SameNumRfi(rn, ro) And SameHandle(rn, ro) And SameContent(rn, ro)))
        addList = NotExistsHandleList.FindAll(Function(rn) rn.NumRfi = 0)
        updateList = ExistsHandleList.FindAll(Function(rn) oldList.Exists(Function(ro) SameNumRfi(rn, ro) And SameHandle(rn, ro) And Not SameContent(rn, ro)))
        'Warning
        badNumberList = NotExistsHandleList.FindAll(Function(rn) rn.NumRfi > oldList.Count)
        changedNumberList = ExistsHandleList.FindAll(Function(rn) oldList.Exists(Function(ro) Not SameNumRfi(rn, ro) And SameHandle(rn, ro)))
        'Confict
        conflictList = NotExistsHandleList.FindAll(Function(rn) rn.NumRfi > 0 And rn.NumRfi < oldList.Count)

        TreeView1.Nodes.Find("UpToDate", True)(0).Nodes.AddRange(upToDateList.ConvertAll(Function(r) New TreeNode(r.Handle)).ToArray)
        TreeView1.Nodes.Find("Add", True)(0).Nodes.AddRange(addList.ConvertAll(Function(r) New TreeNode(r.Handle)).ToArray)
        TreeView1.Nodes.Find("Update", True)(0).Nodes.AddRange(updateList.ConvertAll(Function(r) New TreeNode(r.Handle)).ToArray)
        TreeView1.Nodes.Find("BadNumber", True)(0).Nodes.AddRange(badNumberList.ConvertAll(Function(r) New TreeNode(r.Handle)).ToArray)
        TreeView1.Nodes.Find("ChangedNumber", True)(0).Nodes.AddRange(changedNumberList.ConvertAll(Function(r) New TreeNode(r.Handle)).ToArray)
        TreeView1.Nodes.Find("Conflict", True)(0).Nodes.AddRange(conflictList.ConvertAll(Function(r) New TreeNode(r.Handle)).ToArray)

        For Each n As TreeNode In TreeView1.Nodes.Find("UpToDate", True)(0).Nodes
            n.Checked = True
        Next
        For Each n As TreeNode In TreeView1.Nodes.Find("Add", True)(0).Nodes
            n.Checked = True
        Next
        For Each n As TreeNode In TreeView1.Nodes.Find("Update", True)(0).Nodes
            n.Checked = True
        Next
        For Each n As TreeNode In TreeView1.Nodes.Find("BadNumber", True)(0).Nodes
            n.Checked = True
        Next
        For Each n As TreeNode In TreeView1.Nodes.Find("ChangedNumber", True)(0).Nodes
            n.Checked = True
        Next
    End Sub

    Private Sub OkButton_Click(sender As Object, e As EventArgs) Handles OkButton.Click
        For Each n As TreeNode In TreeView1.Nodes.Find("Add", True)(0).Nodes
            If n.Checked Then
                _Excel.AppendRfi(addList(n.Index))
            End If
        Next
        For Each n As TreeNode In TreeView1.Nodes.Find("Update", True)(0).Nodes
            If n.Checked Then
                Dim newRfi = updateList(n.Index)
                Dim oldRfi = oldList.Find(Function(r) r.Handle = newRfi.Handle)
                oldRfi.SetRfi(newRfi)
            End If
        Next
        For Each n As TreeNode In TreeView1.Nodes.Find("BadNumber", True)(0).Nodes
            If n.Checked Then
                _Excel.AppendRfi(badNumberList(n.Index))
            End If
        Next
        For Each n As TreeNode In TreeView1.Nodes.Find("ChangedNumber", True)(0).Nodes
            If n.Checked Then
                Dim newRfi = changedNumberList(n.Index)
                Dim oldRfi = oldList.Find(Function(r) r.Handle = newRfi.Handle)
                newRfi.NumRfi = oldRfi.NumRfi
                oldRfi.SetRfi(newRfi)
            End If
        Next
        For Each n As TreeNode In TreeView1.Nodes.Find("Conflict", True)(0).Nodes
            If n.Checked Then
                Dim newRfi = conflictList(n.Index)
                Dim oldRfi = oldList.Find(Function(r) r.NumRfi = newRfi.NumRfi)
                oldRfi.SetRfi(newRfi)
            End If
        Next
    End Sub

    Private Sub TreeView1_AfterCheck(sender As Object, e As TreeViewEventArgs) Handles TreeView1.AfterCheck
        If e.Action <> TreeViewAction.Unknown Then
            If e.Node.Nodes.Count > 0 Then
                CheckAllChildNodes(e.Node, e.Node.Checked)
            End If
        End If
    End Sub

    Private Sub CheckAllChildNodes(treeNode As TreeNode, nodeChecked As Boolean)
        Dim node As TreeNode
        For Each node In treeNode.Nodes
            node.Checked = nodeChecked
            If node.Nodes.Count > 0 Then
                CheckAllChildNodes(node, nodeChecked)
            End If
        Next
    End Sub
End Class
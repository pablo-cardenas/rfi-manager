﻿Imports System.Collections.Generic

Public Class Rfi
    Protected _Handle As Long
    Protected _Discipline As String
    Protected _DwgName As String
    Protected _NumRfi As UShort = 0
    Protected _Description As String
    Protected _StatusCode As Byte

    Public Shared ReadOnly PROPLIST As List(Of String) = New List(Of String)({"NumRfi", "Description", "Discipline", "Status", "DwgName", "Handle"})
    Public Shared ReadOnly POSSIBLESTATUS As List(Of String) = New List(Of String)({"PENDIENTE", "EN PROCESO", "COMPLETADO", "NO PROCEDE", "NUEVA"})
    Private Shared ReadOnly POSSIBLEDISCIPLINE As List(Of String) = New List(Of String)({"Arquitectura", "Estructuras"})

    Public Overridable Property Handle() As String
        Get
            Return Hex(_Handle)
        End Get
        Set(value As String)
            _Handle = Convert.ToInt64(value, 16)
        End Set
    End Property

    Public Overridable Property NumRfi() As String
        Get
            Return _NumRfi
        End Get
        Set(value As String)
            UShort.TryParse(value, _NumRfi)
        End Set
    End Property

    Public Overridable Property Description() As String
        Get
            Return _Description
        End Get
        Set(value As String)
            _Description = value
        End Set
    End Property

    Public Overridable Property Discipline() As String
        Get
            Return _Discipline
        End Get
        Set(value As String)
            _Discipline = value
        End Set
    End Property

    Public Overridable Property Status() As String
        Get
            Return POSSIBLESTATUS(_StatusCode)
        End Get
        Set(value As String)
            _StatusCode = POSSIBLESTATUS.IndexOf(value.ToUpper)
            If _StatusCode = -1 Then
                Throw New Exception("'Estado' no válido")
            End If
        End Set
    End Property

    Public Overridable Property DwgName() As String
        Get
            Return _DwgName
        End Get
        Set(value As String)
            _DwgName = value
        End Set
    End Property

    Public Sub SetRfi(r As Rfi)
        For Each prop In PROPLIST
            Dim val = GetType(Rfi).GetProperty(prop).GetValue(r)
            GetType(Rfi).GetProperty(prop).SetValue(Me, val)
        Next
    End Sub
End Class
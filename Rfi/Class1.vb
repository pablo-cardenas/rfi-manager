﻿Imports Autodesk.AutoCAD.ApplicationServices.Core
Imports Autodesk.AutoCAD.EditorInput
Imports Autodesk.AutoCAD.Runtime
Imports Autodesk.AutoCAD.DatabaseServices
Imports System.Collections.Generic
Imports Autodesk.AutoCAD.Windows

Public Class Class1
    Private fileName As String = Nothing

    <CommandMethod("RegistrarRFI", CommandFlags.Modal + CommandFlags.UsePickSet)>
    Public Sub RegisterCommand()
        If fileName Is Nothing Then
            SetFilename()
            If fileName Is Nothing Then
                Exit Sub
            End If
        End If

        Dim ed = Application.DocumentManager.MdiActiveDocument.Editor
        If Not DWGHasName() Then
            'TODO: Älert! Drawing has not name
            ed.WriteMessage(vbLf & "Este documento no tiene nombre.")
            Exit Sub
        End If

        Dim selectionRes = ed.SelectImplied()
        ed.WriteMessage(vbLf & "Archivo: " & fileName)

        ' If there's no pickfirst set available...
        If selectionRes.Status = PromptStatus.Error Then
            ' ... ask the user to select entities
            Dim pKeyOpts = New PromptKeywordOptions(vbLf & "Ingrese una opción")
            pKeyOpts.Keywords.Add("Selección")
            pKeyOpts.Keywords.Add("Todos")
            pKeyOpts.Keywords.Add("CambiarDeArchivo")
            pKeyOpts.Keywords.Default = "Todos"
            pKeyOpts.AllowNone = False

            Dim pKeyRes As PromptResult = ed.GetKeywords(pKeyOpts)
            Select Case pKeyRes.StringResult
                Case "Selección"
                    RegisterSelection()
                Case "Todos"
                    RegisterAll()
                Case "CambiarDeArchivo"
                    SetFilename()
            End Select
        Else
            'TODO: If there then was a pickfirst Set, clear it
            'ed.SetImpliedSelection(New ObjectId(0) {})
            RegisterSelection()
        End If

        Dim acDoc = Application.DocumentManager.MdiActiveDocument
        acDoc.Database.SaveAs(acDoc.Name, True, DwgVersion.Current, acDoc.Database.SecurityParameters)
    End Sub

    Sub RegisterAll()
        Throw New NotImplementedException
    End Sub

    Sub RegisterSelection()
        Dim selRes = PromptForRfiBlocks()
        If (selRes.Status = PromptStatus.OK) Then
            ' There are selected entities
            Register(selRes.Value)
        End If
    End Sub

    Function PromptForRfiBlocks() As PromptSelectionResult
        Dim ed = Application.DocumentManager.MdiActiveDocument.Editor
        'TODO: Create a new filter that only selects RFI block references.
        Dim filList(0) As TypedValue
        filList.SetValue(New TypedValue(DxfCode.Start, "INSERT"), 0)
        Dim filter = New SelectionFilter(filList)
        Dim selectionOpts = New PromptSelectionOptions
        selectionOpts.MessageForAdding = "Seleccionar bloques: "
        Return ed.GetSelection(selectionOpts, filter)
    End Function

    Sub Register(ss As SelectionSet)
        Dim ed = Application.DocumentManager.MdiActiveDocument.Editor
        Dim RfiBlkRefList = GetRfiBlockReference(ss)
        Dim rfiBlockList = RfiBlkRefList.ConvertAll(Function(r) New RfiBlock(r.Handle))
        ed.WriteMessage(vbLf & RfiBlkRefList.Count & " bloques RFI encontrados")
        Using excel = New Excel(fileName)
            excel.RegisterRfiList(rfiBlockList)
        End Using
    End Sub

    Function GetRfiBlockReference(ss As SelectionSet) As List(Of BlockReference)
        Dim db = HostApplicationServices.WorkingDatabase
        Dim idList = New List(Of ObjectId)(ss.GetObjectIds())

        Dim blkRefList As List(Of BlockReference)
        Using tr = db.TransactionManager.StartOpenCloseTransaction()
            blkRefList = idList.ConvertAll(Of BlockReference)(Function(id) tr.GetObject(id, OpenMode.ForRead))
            tr.Commit()
        End Using

        Return blkRefList.FindAll(AddressOf isRFI)
    End Function

    Function isRFI(blkRef As BlockReference) As Boolean
        Dim db = HostApplicationServices.WorkingDatabase
        Dim block As BlockTableRecord
        Using tr = db.TransactionManager.StartOpenCloseTransaction()
            block = tr.GetObject(blkRef.DynamicBlockTableRecord, OpenMode.ForRead)
            tr.Commit()
        End Using
        Return block.Name = "RFI"
    End Function

    Sub SetFilename()
        Dim root = IO.Path.GetPathRoot(Application.DocumentManager.MdiActiveDocument.Name)
        Dim rfiFolder = IO.Path.Combine(root, "_Gestion del Modelo\RFI\")
        Dim SaveFileDialogEx As New SaveFileDialog("Ubicación del Reporte de Consulta", rfiFolder, "xlsx", "Avisen a Pablo si ven eso xD.", 0)
        If SaveFileDialogEx.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            fileName = SaveFileDialogEx.Filename
        Else
            Dim ed = Application.DocumentManager.MdiActiveDocument.Editor
            ed.WriteMessage("Error al obtener archivo")
        End If
    End Sub

    Private Function DWGHasName() As Boolean
        Dim var As Short = Application.GetSystemVariable("DWGTITLED")
        Return var <> 0
    End Function
End Class
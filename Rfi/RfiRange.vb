﻿Imports Microsoft.Office.Interop.Excel

Public Class RfiRange
    Inherits Rfi
    Private _Range As Range

    Public Overrides Property NumRfi As String
        Get
            Return MyBase.NumRfi
        End Get
        Set(value As String)
            MyBase.NumRfi = value
            _Range(Excel.COLUMNS.IndexOf("NumRfi") + 1) = MyBase.NumRfi
        End Set
    End Property

    Public Overrides Property Description As String
        Get
            Return MyBase.Description
        End Get
        Set(value As String)
            MyBase.Description = value
            _Range(Excel.COLUMNS.IndexOf("Description") + 1) = MyBase.Description
        End Set
    End Property

    Public Overrides Property Discipline As String
        Get
            Return MyBase.Discipline
        End Get
        Set(value As String)
            MyBase.Discipline = value
            _Range(Excel.COLUMNS.IndexOf("Discipline") + 1) = MyBase.Discipline
        End Set
    End Property

    Public Overrides Property DwgName As String
        Get
            Return MyBase.DwgName
        End Get
        Set(value As String)
            MyBase.DwgName = value
            _Range(Excel.COLUMNS.IndexOf("DwgName") + 1) = MyBase.DwgName
        End Set
    End Property

    Public Overrides Property Status As String
        Get
            Return MyBase.Status
        End Get
        Set(value As String)
            MyBase.Status = value
            _Range(Excel.COLUMNS.IndexOf("Status") + 1) = MyBase.Status
        End Set
    End Property

    Public Overrides Property Handle As String
        Get
            Return MyBase.Handle
        End Get
        Set(value As String)
            MyBase.Handle = value
            _Range(Excel.COLUMNS.IndexOf("Handle") + 1) = MyBase.Handle
        End Set
    End Property

    Public Sub New(range As Range)
        _Range = range
        _Range(Excel.COLUMNS.IndexOf("Description") + 1, Excel.COLUMNS.IndexOf("Handle") + 1).NumberFormat = "@"
        For Each prop In PROPLIST
            Dim value As String = _Range(Excel.COLUMNS.IndexOf(prop) + 1).Text
            GetType(Rfi).GetProperty(prop).SetValue(Me, value)
        Next
    End Sub
End Class
